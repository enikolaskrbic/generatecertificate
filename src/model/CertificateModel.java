/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Skrbic
 */
public class CertificateModel implements Serializable{
    
    private String aliase;
    private String validTo;
    private String validFrom;
    private List<CertificateModel> childs = new ArrayList<>();
    private String isValidate;

    public CertificateModel(String aliase,  String validFrom,String validTo,String isValidate) {
        this.aliase = aliase;
        this.validTo = validTo;
        this.validFrom = validFrom;
        this.isValidate = isValidate;
    }

    public String getAliase() {
        return aliase;
    }

    public void setAliase(String aliase) {
        this.aliase = aliase;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public List<CertificateModel> getChilds() {
        return childs;
    }

    public void setChilds(List<CertificateModel> childs) {
        this.childs = childs;
    }

    public String getIsValidate() {
        return isValidate;
    }

    public void setIsValidate(String isValidate) {
        this.isValidate = isValidate;
    }

 
    
    
    
}
