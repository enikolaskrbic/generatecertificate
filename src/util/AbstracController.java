/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Igor
 */
public abstract class AbstracController implements ControlledScreen {

    public boolean okClicked;

    public boolean isOkClicked() {
        return okClicked;
    }

    public void setOkClicked(boolean okClicked) {
        this.okClicked = okClicked;
    }

    public abstract void initialize(boolean update, Object objToUpdate);

}
