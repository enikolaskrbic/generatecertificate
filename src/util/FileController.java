/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.Enumeration;
import model.CertificateModel;
import model.User;

/**
 *
 * @author Skrbic
 */
public class FileController {

    public static void saveCertificateModels(ArrayList<CertificateModel> certificateModels) {

        try (OutputStream file = new FileOutputStream("CertificateModelList.bin");
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput out = new ObjectOutputStream(buffer);) {
            out.writeObject(certificateModels);
        } catch (Exception e) {
            return;
        }

    }

    public static ArrayList<CertificateModel> loadCertificateModels() {
        ArrayList<CertificateModel> certificateModels = new ArrayList<>();

        try (InputStream file = new FileInputStream("CertificateModelList.bin");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput in = new ObjectInputStream(buffer);) {
            certificateModels = (ArrayList<CertificateModel>) in.readObject();
        } catch (Exception e) {
            return null;
        }
        return certificateModels;
    }

    public static ArrayList<CertificateModel> loadCertificateModels(KeyStore keyStore) throws KeyStoreException {
        ArrayList<CertificateModel> certificateModels = new ArrayList<>();

        try (InputStream file = new FileInputStream("CertificateModelList.bin");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput in = new ObjectInputStream(buffer);) {
            certificateModels = (ArrayList<CertificateModel>) in.readObject();

        } catch (Exception e) {
            return null;
        }
        ArrayList<CertificateModel> retVal = new ArrayList<>();
        Enumeration enumeration = keyStore.aliases();

        while (enumeration.hasMoreElements()) {
            String alias = (String) enumeration.nextElement();
            for (CertificateModel certificateModel : certificateModels) {
                if(certificateModel.getAliase().equals(alias))
                    retVal.add(certificateModel);
            }
        }

        return retVal;
    }
    
    
    public static void saveUsers(ArrayList<User> users) {

        try (OutputStream file = new FileOutputStream("Users.bin");
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput out = new ObjectOutputStream(buffer);) {
            out.writeObject(users);
        } catch (Exception e) {
            return;
        }

    }
    
    public static ArrayList<User> loadUsers() {
        ArrayList<User> users = new ArrayList<>();

        try (InputStream file = new FileInputStream("Users.bin");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput in = new ObjectInputStream(buffer);) {
            users = (ArrayList<User>) in.readObject();
        } catch (Exception e) {
            return null;
        }
        return users;
    }

}
