/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static generatecertificate.GenerateCertificate.fl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import util.AbstractAlertDialog;
import util.AbstractDialog;
import model.KeyModel;
import model.User;

/**
 *
 * @author Skrbic
 */
public class MainFrameController extends AbstractDialog implements Initializable {

    @FXML
    private Label label;
    @FXML
    private TableView<KeyModel> tblKeystore;
    @FXML
    private TableColumn<KeyModel, String> tbcName;
    @FXML
    private Button btnCreateKeystore;

    private ObservableList<KeyModel> keyList = FXCollections.observableArrayList();
    private KeyModel keyCollection;
    private User user;
    @FXML
    private Button btnCreateCertificate;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initKeyList();
        initTable();
        initTableController();
    }

    private void initKeyList() {
        keyList.clear();
        File folder = new File("keyStores");
        File[] keyStoreFiles = folder.listFiles();
        for (File keyStoreFile : keyStoreFiles) {
            KeyModel keyCollection = new KeyModel();
            keyCollection.setName(keyStoreFile.getName().split(".jks")[0]);
            keyList.add(keyCollection);
        }
    }
    
    public void setUser(User user){
        this.user = user;
        initPrivilegie();
    }
    
    private void initPrivilegie(){
        switch(user.getRole()){
            case ROLE_ADMIN:
                break;
            case ROLE_GUEST:
                btnCreateCertificate.setDisable(true);
                btnCreateKeystore.setDisable(true);
                break;
            case ROLE_OWNER:
                btnCreateKeystore.setDisable(true);
                break;
        }
    }

    private void initTable() {
        tbcName.setCellValueFactory(new PropertyValueFactory<KeyModel, String>("name"));
        tblKeystore.setItems(keyList);
    }

    private void initTableController() {
        tblKeystore.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                keyCollection = (KeyModel) newValue;

                tblKeystore.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        if (newValue != null) {
                            if (t.getClickCount() == 2) {
                                try {
                                    setModalDialog("/view/CreateKeystore.fxml");
                                } catch (IOException ex) {
                                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                CreateKeystore t2 = (CreateKeystore) fl.getController();
                                t2.setStage(mf);
                                t2.setUsername(keyCollection.getName());
                                //t.initialize(null,null);
                                mf.setTitle("Create keystore");
                                mf.setScene(scene);
                                mf.showAndWait();
                                if (t2.isOkClicked()) {
                                    try {
                                        KeyStore keyStore = checkKeyStore(keyCollection.getName(), t2.getPasswordField().getText());
                                        openListCertificate(keyCollection.getName(),keyStore, t2.getPasswordField().getText());
                                       
                                    } catch (Exception ex) {
                                        AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", "Password is incorect.", Alert.AlertType.ERROR);

                                    }
                                }
                            }
                        }

                    }
                });
            }
        });
    }

    private void openListCertificate(String keyName,KeyStore keyStore , String passwordField) {
        try {
            setModalDialog("/view/ListCertificates.fxml");
        } catch (IOException ex) {
            Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ListCertificatesController t2 = (ListCertificatesController) fl.getController();
        t2.setStage(mf);
        
        t2.setUser(user);
        //t.initialize(null,null);
        mf.setTitle("List of certificates for: "+keyName);
        mf.setScene(scene);
        t2.initData(keyName,keyStore, passwordField);
        mf.showAndWait();
    }

    private KeyStore checkKeyStore(String fileName, String password) throws KeyStoreException, NoSuchProviderException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException {
        KeyStore keyStore = KeyStore.getInstance("JKS", "SUN");
        FileInputStream file = new FileInputStream("keyStores/" + fileName + ".jks");
        keyStore.load(file, password.toCharArray());
        return keyStore;
    }

    @FXML
    private void createKeystore(ActionEvent event) throws IOException {

        setModalDialog("/view/CreateKeystore.fxml");
        CreateKeystore t = (CreateKeystore) fl.getController();
        t.setStage(mf);
        //t.initialize(null,null);
        t.initListeners();
        mf.setTitle("Create keystore");
        mf.setScene(scene);
        mf.showAndWait();
        
        initKeyList();
    }

    @FXML
    private void createCertificate(ActionEvent event) throws IOException {
        setModalDialog("/view/CreateCertificate.fxml");
        CreateCertificate t = (CreateCertificate) fl.getController();
        t.setStage(mf);
        t.setUser(user);
        //t.initialize(null,null);
        mf.setTitle("Create certificate");
        mf.setScene(scene);
        mf.show();
    }

}
