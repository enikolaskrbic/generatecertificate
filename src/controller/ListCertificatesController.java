/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.CertificateModel;
import model.KeyModel;
import model.User;
import security.KeyStoreWriter;
import util.AbstractAlertDialog;
import util.AbstractDialog;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class ListCertificatesController extends AbstractDialog implements Initializable {

    @FXML
    private TableView<CertificateModel> tblCertificates;
    @FXML
    private TableColumn<CertificateModel, String> tbcAlias;
    @FXML
    private TableColumn<CertificateModel, String> tbcValidFrom;
    @FXML
    private TableColumn<CertificateModel, String> tbcValidTo;
    @FXML
    private TableColumn<CertificateModel, String> tbcIsValid;
    @FXML
    private Button btnRetire;
    
    private ObservableList<CertificateModel> certList = FXCollections.observableArrayList();
    private CertificateModel certificateModel;
    private KeyStore keyStore;
    private String passwordField;
    private String keyStoreName;
    private User user;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    public void setUser(User user){
        this.user = user;
        initPrivilegie();
    }
    private void initPrivilegie(){
        switch(user.getRole()){
            case ROLE_ADMIN:
                break;
            case ROLE_GUEST:
                btnRetire.setDisable(true);
                break;
            case ROLE_OWNER:
                btnRetire.setDisable(true);
                break;
        }
    }

    public void initData(String keyStoreName, KeyStore keyStore, String passwordField) {
        certList.clear();
        this.keyStore = keyStore;
        this.keyStoreName = keyStoreName;
        this.passwordField = passwordField;
        try {
            Enumeration enumeration = keyStore.aliases();

            while (enumeration.hasMoreElements()) {
                String alias = (String) enumeration.nextElement();
                X509Certificate cert = (X509Certificate) keyStore.getCertificate(alias);

                String validFrom = cert.getNotBefore().toString();
                String validTo = cert.getNotAfter().toString();
                try {
                    cert.checkValidity();
                    certList.add(new CertificateModel(alias, validFrom, validTo,"YES"));
                } catch (CertificateExpiredException ex) {
                    //continue;
                    certList.add(new CertificateModel(alias, validFrom, validTo,"NO"));
                } catch (CertificateNotYetValidException ex) {
                    //continue;
                    certList.add(new CertificateModel(alias, validFrom, validTo,"NO"));
                }

            }
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        initTable();
        initTableController();
    }

    private void initTable() {
        tbcAlias.setCellValueFactory(new PropertyValueFactory<CertificateModel, String>("aliase"));
        tbcValidFrom.setCellValueFactory(new PropertyValueFactory<CertificateModel, String>("validFrom"));
        tbcValidTo.setCellValueFactory(new PropertyValueFactory<CertificateModel, String>("validTo"));
        tbcIsValid.setCellValueFactory(new PropertyValueFactory<CertificateModel, String>("isValidate"));
        tblCertificates.setItems(certList);
    }

    private void initTableController() {
        tblCertificates.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                certificateModel = (CertificateModel) newValue;
                if (certificateModel == null) {
                    return;
                }
                tblCertificates.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        if (newValue != null) {
                            if (t.getClickCount() == 2) {
                                try {
                                    setModalDialog("/view/CreateKeystore.fxml");
                                } catch (IOException ex) {
                                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                CreateKeystore t2 = (CreateKeystore) fl.getController();
                                t2.setStage(mf);
                                t2.setUsername(certificateModel.getAliase());
                                //t.initialize(null,null);
                                mf.setTitle("Insert password");
                                mf.setScene(scene);
                                mf.showAndWait();
                                if (t2.isOkClicked()) {
                                    try {
                                        openAboutCertificate(certificateModel.getAliase(), t2.getPasswordField());
//                                        KeyStore keyStore = checkKeyStore(keyCollection.getName(), t2.getPasswordField().getText());
//                                        openListCertificate(keyStore);

                                    } catch (Exception ex) {
//                                        AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", "Password is incorect.", Alert.AlertType.ERROR);

                                    }
                                }
                            }
                        }

                    }
                });
            }
        });
    }

    private void openAboutCertificate(String alias, PasswordField passwordField) {
        try {
            if (keyStore.isKeyEntry(alias)) {
                Certificate certificate = keyStore.getCertificate(alias);
                PrivateKey privKey = null;
                try {
                    privKey = (PrivateKey) keyStore.getKey(alias, passwordField.getText().toCharArray());
                    openDialogAbout(certificate, alias);
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    AbstractAlertDialog a = new AbstractAlertDialog("Error", "Certificate error!", "Passwort is incorect.", Alert.AlertType.ERROR);
                    return;

                }
            } else {
                AbstractAlertDialog a = new AbstractAlertDialog("Error", "Certificate error!", "Certificate dosen't exist.", Alert.AlertType.ERROR);
                return;
            }

        } catch (KeyStoreException ex) {
            Logger.getLogger(ListCertificatesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void openDialogAbout(Certificate certificate, String alias) {
        try {
            setModalDialog("/view/AboutCertificate.fxml");
        } catch (IOException ex) {
            Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
        }
        AboutCertificate t2 = (AboutCertificate) fl.getController();
        t2.setStage(mf);
        t2.initData(certificate);
        //t.initialize(null,null);
        mf.setTitle("About certificate : " + alias);
        mf.setScene(scene);
        mf.showAndWait();
    }

    @FXML
    private void retire(ActionEvent event) {
        if (certificateModel != null) {
            retireCertificate(certificateModel, keyStore);
            //initData(keyStoreName, keyStore, passwordField);
            stage.close();
        }
    }

    private void retireCertificate(CertificateModel certificateModel, KeyStore kStore) {
        try {
            for (CertificateModel child : certificateModel.getChilds()) {
                retireCertificate(child, kStore);
            }
            kStore.deleteEntry(certificateModel.getAliase());
        } catch (KeyStoreException ex) {
            Logger.getLogger(ListCertificatesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        KeyStoreWriter keyStoreWritter = new KeyStoreWriter();
        keyStoreWritter.loadKeyStore(null, passwordField.toCharArray());
        keyStoreWritter.saveKeyStore("keyStores/" + keyStoreName + ".jks", passwordField.toCharArray());

    }

}
