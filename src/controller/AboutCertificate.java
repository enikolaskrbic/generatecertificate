/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import util.AbstractDialog;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class AboutCertificate extends AbstractDialog implements Initializable {

    @FXML
    private TextField tbxVersion;
    @FXML
    private TextField tbxIssuer;
    @FXML
    private TextField tbxSerialNumber;
    @FXML
    private TextField tbxValidFrom;
    @FXML
    private TextField tbxValidTo;
    @FXML
    private TextField publicKeyAlg;
    @FXML
    private TextField tbxSignatureAlg;
    @FXML
    private TextField tbxIsValidate;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tbxVersion.setEditable(false);
        tbxIssuer.setEditable(false);
        tbxSerialNumber.setEditable(false);
        tbxValidFrom.setEditable(false);
        tbxValidTo.setEditable(false);
        publicKeyAlg.setEditable(false);
        tbxSignatureAlg.setEditable(false);
        tbxIsValidate.setEditable(false);
    }    
    
    public void initData(Certificate certificate){
        
        X509Certificate xCertificate509 =(X509Certificate)certificate;
        tbxVersion.setText(String.valueOf(xCertificate509.getVersion()));
        tbxIssuer.setText(xCertificate509.getIssuerDN().toString());
        tbxSerialNumber.setText(xCertificate509.getSerialNumber().toString());
        tbxValidFrom.setText(xCertificate509.getNotBefore().toString());
        tbxValidTo.setText(xCertificate509.getNotAfter().toString());
        publicKeyAlg.setText(xCertificate509.getPublicKey().getAlgorithm().toString()+" (1024)");
        tbxSignatureAlg.setText(xCertificate509.getSigAlgName());
        try {
            xCertificate509.checkValidity();
            tbxIsValidate.setText("YES");
        } catch (CertificateExpiredException ex) {
            tbxIsValidate.setText("NO");
        } catch (CertificateNotYetValidException ex) {
            tbxIsValidate.setText("NO");
        }
    }
    
}
