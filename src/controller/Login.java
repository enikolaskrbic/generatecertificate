/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import enums.Role;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.User;
import security.pass.PasswordHandling;
import util.AbstractDialog;
import util.FileController;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class Login extends AbstractDialog implements Initializable {

    @FXML
    private TextField tbxUsername;
    @FXML
    private PasswordField passField;
    @FXML
    private Button btnLogin;
    @FXML
    private Label lblInfoError;
    private ArrayList<User> users;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        users = FileController.loadUsers();
        if (users == null) {
            users = new ArrayList<>();
            addUser("admin", "admin", Role.ROLE_ADMIN);
            addUser("owner", "owner", Role.ROLE_OWNER);
            addUser("guest", "guest", Role.ROLE_GUEST);
            FileController.saveUsers(users);
            users = FileController.loadUsers();
        }
       
    }
    
    public void initListeners(){
        changeTbxListener();
        setKeyEvents();
    }

    @FXML
    private void Exit(ActionEvent event) {
        stage.close();
    }

    @FXML
    private void Prijava(ActionEvent event) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {

        String korisnickoIme = tbxUsername.getText();
        String passString = passField.getText();

        if (!checkFields(korisnickoIme, passString)) {
            lblInfoError.setText("Popunite sva polja!");
            lblInfoError.setVisible(true);
        } else {
            for (User user : users) {
                if (user.getUsername().equals(korisnickoIme.trim())
                        && PasswordHandling.authenticate(passString, user.getHashPassword(), user.getSalt())) {

                    stage.close();
                    setModalDialog("/view/MainFrame.fxml");
                    MainFrameController t = (MainFrameController) fl.getController();
                    t.setStage(mf);

                    t.setUser(user);
                    mf.setTitle("Generate certificate");
                    //mf.setResizable(false);
                    //mf.getIcons().add(new Image("/images/firangeIcon.png"));
                    mf.setScene(scene);

                    mf.show();
                    return;
                }
            }
            lblInfoError.setText("Neuspela prijava na sistem!");
            lblInfoError.setVisible(true);

        }
    }

    private void setKeyEvents() {
        if (stage != null) {
            stage.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
                if (t.getCode() == KeyCode.ENTER) {
                    try {
                        Prijava(new ActionEvent());
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvalidKeySpecException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (t.getCode() == KeyCode.ESCAPE) {
                    stage.close();
                }
            });
        }

    }

    private boolean checkFields(String korisnickoIme, String lozinka) {
        if (korisnickoIme.trim().equals("") || lozinka.equals("")) {
            return false;
        }
        return true;
    }

    private void changeTbxListener() {
        tbxUsername.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != newValue) {
                lblInfoError.setVisible(false);
            }
        });
        passField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != newValue) {
                lblInfoError.setVisible(false);
            }
        });
    }

    private void addUser(String username, String password, Role role) {
        User user = new User();
        user.setUsername(username);
        user.setRole(role);

        try {
            user.setSalt(PasswordHandling.generateSalt());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            user.setHashPassword(PasswordHandling.hashPassword(password, user.getSalt()));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        users.add(user);
    }

}
