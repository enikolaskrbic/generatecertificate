/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import security.KeyStoreWriter;
import util.AbstractAlertDialog;
import util.AbstractDialog;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class CreateKeystore extends AbstractDialog implements Initializable {

    @FXML
    private TextField tbxUsername;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;
    @FXML
    private PasswordField password;
    
    private KeyStoreWriter keyStoreWritter;
    private boolean update = false;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setOkClicked(false);
        keyStoreWritter = new KeyStoreWriter();
    }  
    
    public void setUsername(String username){
        update = true;
        tbxUsername.setText(username);
        tbxUsername.setDisable(true);
        setKeyEvents();
        
    }
    public void initListeners(){
        setKeyEvents();
    }
   

    @FXML
    private void cancel(ActionEvent event) {
        setOkClicked(false);
        stage.close();
    }

    @FXML
    private void submit(ActionEvent event) {
        String username = tbxUsername.getText();
        String password = this.password.getText();
        if(username.isEmpty() || password.isEmpty()){
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Fields error!", "Fields can't be empty.", Alert.AlertType.ERROR);
            return;
        }
        if(update){
            setOkClicked(true);
            stage.close();
            return;
        }
        keyStoreWritter.loadKeyStore(null, password.toCharArray());
        keyStoreWritter.saveKeyStore("keyStores/"+username+".jks", password.toCharArray());	
        setOkClicked(true);
        stage.close();
    }
    
    public PasswordField  getPasswordField(){
        return password;
    }
    
     private void setKeyEvents() {
        if (stage != null) {
            stage.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
                if (t.getCode() == KeyCode.ENTER) {
                    submit(new ActionEvent());
                }
                if (t.getCode() == KeyCode.ESCAPE) {
                    stage.close();
                }
            });
        }

    }
    
    
    
}
