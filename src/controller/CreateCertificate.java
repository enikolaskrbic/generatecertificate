/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import enums.Role;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.CertificateModel;
import model.User;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.CertIOException;
import security.CertificateGenerator;
import security.IssuerData;
import security.SubjectData;
import util.AbstractAlertDialog;
import util.AbstractDialog;
import util.FileController;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class CreateCertificate extends AbstractDialog implements Initializable {

    @FXML
    private TextField tbxSurname;
    @FXML
    private TextField tbxCommonName;
    @FXML
    private TextField tbxOrganizationUnit;
    @FXML
    private TextField tbxOrganizationName;
    @FXML
    private TextField tbxName;
    @FXML
    private TextField tbxCountryCode;
    @FXML
    private TextField tbxEmail;
    @FXML
    private TextField tbxDateStart;
    @FXML
    private TextField tbxDateEnd;
    @FXML
    private TextField tbxAlias;
    @FXML
    private ComboBox<String> cbxKeystore;
    @FXML
    private ComboBox<String> cbxCA;
    @FXML
    private PasswordField tbxPassword;
    @FXML
    private CheckBox chbCA;

    private Certificate certificate;
    private KeyStore keyStore;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    private String keyPassword = null;
    private PrivateKey privateKey = null;
    private String CertificatePassword = null;
    private User user;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initComboKeystore();
        changeCbxKeyStore();
        changeCbxCA();

    }

    private void initComboKeystore() {
        cbxKeystore.getItems().clear();
        File folder = new File("keyStores");
        File[] keyStoreFiles = folder.listFiles();

        this.cbxKeystore.getItems().add("");
        for (File keyStoreFile : keyStoreFiles) {
            this.cbxKeystore.getItems().add(keyStoreFile.getName().split(".jks")[0]);
        }
    }
    
    public void setUser(User user){
        this.user=user;
        initPrivilegie();
    }
    
    private void initPrivilegie(){
        switch(user.getRole()){
            case ROLE_ADMIN:
                break;
            case ROLE_GUEST:
                
                break;
            case ROLE_OWNER:
                chbCA.setDisable(true);
                break;
        }
    }

    private void initCA(KeyStore keyStore) throws KeyStoreException {
        cbxCA.getItems().clear();
        if(user.getRole()==Role.ROLE_ADMIN)
            this.cbxCA.getItems().add("SELF SIGNED");
//        ArrayList<CertificateModel> certificateModels = FileController.loadCertificateModels(keyStore);
//        for (CertificateModel certificateModel : certificateModels) {
//            Certificate cert = keyStore.getCertificate(certificateModel.getAliase());
//            X509Certificate xCertificate509 = (X509Certificate) cert;
//             if (xCertificate509.getBasicConstraints() != -1) {
//                 this.cbxCA.getItems().add(certificateModel.getAliase());
//             }
//        }
        try {
            Enumeration enumeration = keyStore.aliases();
            while (enumeration.hasMoreElements()) {
                String alias = (String) enumeration.nextElement();
                Certificate cert = keyStore.getCertificate(alias);
                X509Certificate xCertificate509 = (X509Certificate) cert;
                if (xCertificate509.getBasicConstraints() != -1) {
                    try {
                        xCertificate509.checkValidity();
                        this.cbxCA.getItems().add(alias);
                    } catch (CertificateExpiredException ex) {
                        continue;
                    } catch (CertificateNotYetValidException ex) {
                        continue;
                    }
                }
            }
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void changeCbxKeyStore() {
        cbxKeystore.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                // initCA();
                try {
                    String keyUser = (String) t1;
                    if (keyUser == null || keyUser.equals("")) {
                        return;
                    }
                    if (cbxKeystore.getSelectionModel().getSelectedIndex() == 0) {
                        cbxCA.getItems().clear();
                        return;
                    } else {

                        try {
                            setModalDialog("/view/CreateKeystore.fxml");
                        } catch (IOException ex) {
                            Logger.getLogger(CreateCertificate.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        CreateKeystore t2 = (CreateKeystore) fl.getController();
                        t2.setStage(mf);
                        t2.setUsername(keyUser);

                        mf.setTitle("Insert password");
                        mf.setScene(scene);
                        mf.showAndWait();
                        if (t2.isOkClicked()) {

                            try {
                                keyPassword = null;
                                privateKey = null;
                                keyStore = checkKeyStore(keyUser, t2.getPasswordField().getText());
                                keyPassword = t2.getPasswordField().getText();
                            } catch (KeyStoreException ex) {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", " Keystore was tampered with, or password was incorrect", Alert.AlertType.ERROR);
                                initComboKeystore();
                                cbxCA.getItems().clear();

                            } catch (NoSuchProviderException ex) {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", " Keystore was tampered with, or password was incorrect", Alert.AlertType.ERROR);
                                initComboKeystore();
                                cbxCA.getItems().clear();
                            } catch (IOException ex) {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", " Keystore was tampered with, or password was incorrect", Alert.AlertType.ERROR);
                                initComboKeystore();
                                cbxCA.getItems().clear();

                            } catch (NoSuchAlgorithmException ex) {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", " Keystore was tampered with, or password was incorrect", Alert.AlertType.ERROR);
                                initComboKeystore();
                                cbxCA.getItems().clear();
                            } catch (CertificateException ex) {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Password error!", " Keystore was tampered with, or password was incorrect", Alert.AlertType.ERROR);
                                initComboKeystore();
                                cbxCA.getItems().clear();
                            }

                            initCA(keyStore);
                        } else {
                            initComboKeystore();
                            keyPassword = null;
                        }

                    }
                } catch (Exception e) {

                }

            }
        });
    }

    private void changeCbxCA() {
        cbxCA.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                certificate = null;
                String alias = "";
                try {
                    alias = cbxCA.getSelectionModel().getSelectedItem();
                    certificate = keyStore.getCertificate(alias);
                    X509Certificate xCertificate509 = (X509Certificate) certificate;
                    if (xCertificate509 != null) {
                        if (xCertificate509.getBasicConstraints() == -1) {
                            AbstractAlertDialog aad = new AbstractAlertDialog("Warrning!", "CA warrning.", "This is not CA certificate!", Alert.AlertType.ERROR);
                            certificate = null;
                        }
                    }
                } catch (KeyStoreException ex) {
                    //Logger.getLogger(CreateCertificate.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (certificate != null) {
                    try {
                        setModalDialog("/view/CreateKeystore.fxml");
                    } catch (IOException ex) {
                        Logger.getLogger(CreateCertificate.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    CreateKeystore t2 = (CreateKeystore) fl.getController();
                    t2.setStage(mf);
                    t2.setUsername(alias);

                    mf.setTitle("Insert password");
                    mf.setScene(scene);
                    mf.showAndWait();
                    if (t2.isOkClicked()) {
                        //CheckCertificateDialog(keyStoreName, keyStorePassword, keyStore, alias);  
                        //ucitavamo podatke
                        BufferedInputStream in;
                        try {
                            if (keyStore.isKeyEntry(alias)) {
                                Certificate cert = keyStore.getCertificate(alias);
                                privateKey = null;
                                try {
                                    CertificatePassword = null;
                                    privateKey = (PrivateKey) keyStore.getKey(alias, t2.getPasswordField().getText().toCharArray());
                                    CertificatePassword = t2.getPasswordField().getText();

                                } catch (Exception e1) {
                                    // TODO Auto-generated catch block
                                    AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Certificate error.", "Password is incorect!", Alert.AlertType.ERROR);

                                }
                            } else {
                                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Certificate error.", "Certificate dosen't exist.", Alert.AlertType.ERROR);

                            }
                        } catch (KeyStoreException ex) {

                        }
                    } else if (keyStore != null) {
                        try {
                            initCA(keyStore);
                        } catch (KeyStoreException ex) {
                            Logger.getLogger(CreateCertificate.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        cbxCA.getItems().clear();
                    }
                }

            }
        });
    }

    private KeyStore checkKeyStore(String fileName, String password) throws KeyStoreException, NoSuchProviderException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException {
        KeyStore keyStore = KeyStore.getInstance("JKS", "SUN");
        FileInputStream file = new FileInputStream("keyStores/" + fileName + ".jks");
        keyStore.load(file, password.toCharArray());
        return keyStore;
    }

    public boolean validate() {
        if (tbxSurname.getText().isEmpty()
                || tbxCommonName.getText().isEmpty()
                || tbxOrganizationUnit.getText().isEmpty()
                || tbxOrganizationName.getText().isEmpty()
                || tbxCountryCode.getText().isEmpty()
                || tbxEmail.getText().isEmpty()
                || tbxDateStart.getText().isEmpty()
                || tbxDateEnd.getText().isEmpty()
                || tbxAlias.getText().isEmpty()
                || tbxPassword.getText().isEmpty()
                || cbxKeystore.getSelectionModel().getSelectedItem() == null
                || cbxKeystore.getSelectionModel().getSelectedIndex() == -1
                || cbxCA.getSelectionModel().getSelectedItem() == null
                || cbxCA.getSelectionModel().getSelectedIndex() == -1) {
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Fields empty!", "Fields can't be empty.", Alert.AlertType.ERROR);
            return false;
        }

        try {
            sdf.parse(tbxDateStart.getText());
            sdf.parse(tbxDateEnd.getText());
        } catch (ParseException ex) {
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Date format error!", "Format must be dd.MM.yyyy formated.", Alert.AlertType.ERROR);
            return false;
        }

        if (keyPassword == null) {
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Credentials error.", "Credential for keyStore or Certificate is incorect.", Alert.AlertType.ERROR);
            return false;
        }
        if (!cbxCA.getSelectionModel().getSelectedItem().equals("")
                && !cbxCA.getSelectionModel().getSelectedItem().equals("SELF SIGNED")) {
            if (privateKey == null) {
                AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Credentials error.", "Credential for keyStore or Certificate is incorect.", Alert.AlertType.ERROR);
                return false;
            }
        }

        return true;
    }

    @FXML
    private void cancel(ActionEvent event) {
        this.stage.close();
    }

    @FXML
    private void submit(ActionEvent event) throws CertIOException {
        if (validate()) {
            createCertificate();
            setOkClicked(true);
            this.stage.close();
        }
    }

    private void createCertificate() throws CertIOException {

        CertificateGenerator generator = new CertificateGenerator();

        KeyPair keyPair = generator.generateKeyPair();
        X500NameBuilder builder = generateBuilder();
        int snb = new Double(Math.random() * 100000).intValue();
        String certificateNumber = Integer.toString(snb);
        Date dateStart = null;
        Date dateEnd = null;
        try {
            dateStart = sdf.parse(tbxDateStart.getText());
            dateEnd = sdf.parse(tbxDateEnd.getText());
        } catch (Exception e) {
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Error", "Date format error!", "Format must be dd.MM.yyyy formated.", Alert.AlertType.ERROR);
            return;
        }

        IssuerData issuerData = null;

        if (certificate != null) {
            String alias = "";
            try {
                alias = keyStore.getCertificateAlias(certificate);
            } catch (KeyStoreException e2) {
                // TODO Auto-generated catch block
                e2.printStackTrace();
            }
            X500NameBuilder buildertmp = parseDataFromCertificate((X509Certificate) certificate);
            issuerData = new IssuerData(privateKey, buildertmp.build());

        } else {
            issuerData = new IssuerData(keyPair.getPrivate(), builder.build());
        }

        SubjectData subjectData = new SubjectData(keyPair.getPublic(), builder.build(), certificateNumber, dateStart, dateEnd);
        boolean isCA=chbCA.isSelected();
        if(certificate==null){
            isCA=true;
        }
        
        X509Certificate cert = generator.generateCertificate(issuerData, subjectData,isCA);

        try {
            keyStore.setKeyEntry(tbxAlias.getText(), keyPair.getPrivate(), tbxPassword.getText().toCharArray(), new X509Certificate[]{cert});

            FileOutputStream outputFile = new FileOutputStream("keyStores/" + (String) cbxKeystore.getSelectionModel().getSelectedItem() + ".jks");
            keyStore.store(outputFile, keyPassword.toCharArray());
        } catch (Exception e1) {

            e1.printStackTrace();
        }
        saveCertificate(cert, tbxAlias.getText());

        String isValidate = "YES";
        try {
            cert.checkValidity();
        } catch (CertificateExpiredException ex) {
           isValidate = "NO";
        } catch (CertificateNotYetValidException ex) {
           isValidate = "NO";
        }
        
        CertificateModel certificateModel = new CertificateModel(tbxAlias.getText(), tbxDateStart.getText(), tbxDateEnd.getText(),isValidate);
        ArrayList<CertificateModel> certificateModels = FileController.loadCertificateModels();
        if (certificateModels == null) {
            certificateModels = new ArrayList<>();
        }
        certificateModels.add(certificateModel);
        if (certificate != null) {
            for (CertificateModel certificateModel1 : certificateModels) {
                if (certificateModel1.getAliase().equals(cbxCA.getSelectionModel().getSelectedItem())) {
                    certificateModel1.getChilds().add(certificateModel);
                    return;
                }
            }
        }
        FileController.saveCertificateModels(certificateModels);

    }

    private X500NameBuilder generateBuilder() {

        X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
        builder.addRDN(BCStyle.CN, tbxCommonName.getText());
        builder.addRDN(BCStyle.SURNAME, tbxSurname.getText());
        builder.addRDN(BCStyle.NAME, tbxName.getText());
        builder.addRDN(BCStyle.O, tbxOrganizationName.getText());
        builder.addRDN(BCStyle.OU, tbxOrganizationUnit.getText());
        builder.addRDN(BCStyle.C, tbxCountryCode.getText());
        builder.addRDN(BCStyle.E, tbxEmail.getText());
        int uid = new Double(Math.random() * 100000).intValue();
        builder.addRDN(BCStyle.UID, Integer.toString(uid));
        return builder;

    }

    public X500NameBuilder parseDataFromCertificate(X509Certificate certificateForParse) {

        String stringForParse = certificateForParse.toString();

        String[] arrayForParse = stringForParse.split("Issuer:")[1].split(",");
        String UID = arrayForParse[0].split("=")[1];
        String EMAILADDRESS = arrayForParse[1].split("=")[1];
        String C = arrayForParse[2].split("=")[1];
        String OU = arrayForParse[3].split("=")[1];
        String O = arrayForParse[4].split("=")[1];
        String NAME = arrayForParse[5].split("=")[1];
        String SURNAME = arrayForParse[6].split("=")[1];
        String CN = arrayForParse[7].split("=")[1].split("\n")[0];

        X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
        builder.addRDN(BCStyle.CN, CN);
        builder.addRDN(BCStyle.SURNAME, SURNAME);
        builder.addRDN(BCStyle.NAME, NAME);
        builder.addRDN(BCStyle.O, O);
        builder.addRDN(BCStyle.OU, OU);
        builder.addRDN(BCStyle.C, C);
        builder.addRDN(BCStyle.E, EMAILADDRESS);
        builder.addRDN(BCStyle.UID, UID);

        return builder;
    }

    private void saveCertificate(X509Certificate cert, String alias) {

        byte[] buf;
        try {
            buf = cert.getEncoded();
            FileOutputStream os;
            os = new FileOutputStream("certificate/" + alias + ".cer");
            os.write(buf);
            os.close();
        } catch (CertificateEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
