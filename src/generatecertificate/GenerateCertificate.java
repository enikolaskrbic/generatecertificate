/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecertificate;

import controller.Login;
import controller.MainFrameController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Skrbic
 */
public class GenerateCertificate extends Application {
    public static FXMLLoader fl;
    @Override
    public void start(Stage stage) throws Exception {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource("/view/Login.fxml"));
        fl.load();
        Parent root = fl.getRoot();
       
        
        Login tc = (Login) fl.getController();

        tc.setStage(stage);
        //tc.initialize(null,null);
        tc.initListeners();
        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.initStyle(StageStyle.TRANSPARENT);
        //stage.initStyle(StageStyle.TRANSPARENT);
        //stage.getIcons().add(new Image("/images/firangeIcon.png"));
        stage.setScene(scene);
        stage.show();
    }

    /**s
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
    
}
